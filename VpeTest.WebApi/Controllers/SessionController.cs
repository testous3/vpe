﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Swashbuckle.Swagger.Annotations;
using VpeTest.Model;
using VpeTest.Service;

namespace VpeTest.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SessionController : ApiController
    {
        private ISessionService _sessionService;
        public SessionController(ISessionService sessionService)
        {
            this._sessionService = sessionService;
        }

        // GET api/values
        [SwaggerOperation("GetSessionByDate")]
        [Route("api/Session/GetSessionByDate")]
        public IHttpActionResult GetSessionByDate(DateTime date)
        {
            try
            {
                return Ok(_sessionService.GetSession(date));

            }
            catch (Exception ex)
            {
                //TO DO - Add Logger 

                return BadRequest(ex.Message);
            }
        }

        // GET api/values
        [SwaggerOperation("GetSessionViewModelByDate")]
        [Route("api/Session/GetSessionViewModelByDate")]
        public IHttpActionResult GetSessionViewModelByDate(DateTime date)
        {
            try
            {
                return Ok(_sessionService.GetSessionViewModelForUI(date));
            }
            catch (Exception ex)
            {
                //TO DO - Add Logger 
                return BadRequest(ex.Message);
            }
        }

    }
}
