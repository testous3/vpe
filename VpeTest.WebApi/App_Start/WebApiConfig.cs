﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity;
using Unity.AspNet.WebApi;
using VpeTest.Repository;
using VpeTest.Service;

namespace VpeTest.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.EnableCors();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            RegisterComponents(config);

        }
        public static void RegisterComponents(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterType<ISessionPriceRepository, MockSessionPriceRepository>();
            container.RegisterType<ISessionService, SessionService>();
            config.DependencyResolver = new UnityDependencyResolver(container);
        }
    }

}

