import { Component } from '@angular/core';
import {SessionService}    from './session.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public Data:any;
  public SelectDate= new Date();
  constructor(private sessionservice:SessionService){
    
    }
  title = 'VpeTest-AngularApp';
  
  OnDateSessionChange()
  {
      this.Data =   this.sessionservice.getSessionsViewModelByDate(this.SelectDate).subscribe(data => {
          this.Data=data;
           console.log(this.Data);
        });
     
  }

}
