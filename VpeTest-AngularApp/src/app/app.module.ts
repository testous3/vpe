import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {SessionService}    from './session.service'
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
    
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,HttpClientModule
  ],
  providers: [SessionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
