﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VpeTest.Model;
using VpeTest.Repository;
using VpeTest.Service;

namespace VpeTest.UnitTest
{
    [TestClass]
    public class SessionRepositoryTest
    {
        [TestMethod]
        public void GetSessionModelForUI_TestMethod()
        {
            // Arrange
            var mockData = new List<SessionPrice>();
            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Normal",
                Zone = "Balcon",
                Price = 50
            });
            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Enfant",
                Zone = "Balcon",
                Price = 45
            });

            var mockDate = DateTime.Now;

            var repoMock = new Mock<ISessionPriceRepository>();

            repoMock.Setup(r => r.GetAllSessionPrices(mockDate)).Returns(mockData);

            ISessionService sessionRepo = new SessionService(repoMock.Object);

            //ACTE 
            var nbreCategories = sessionRepo.GetSessionViewModelForUI(mockDate).Count();
            var nbreZoneOfCarreOr = sessionRepo.GetSessionViewModelForUI(mockDate)
                .First(c => c.Category == "Carré Or").Zones.Count();
            //Assert
            Assert.IsTrue(nbreCategories == 1);  // La categorie "Carré Or"
            Assert.AreEqual(nbreZoneOfCarreOr, 1);  // La zone "Balcon"

        }

        [TestMethod]
        public void TEST_Categories_ORDER()
        {
            // Arrange
            var mockData = new List<SessionPrice>();
            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Normal",
                Zone = "Balcon",
                Price = 50
            });
            mockData.Add(new SessionPrice()
            {
                Category = "Catégorie 1",
                Tarif = "Enfant",
                Zone = "Balcon",
                Price = 100
            });

            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Enfant",
                Zone = "Mezzanine",
                Price = 70
            });

            var mockDate = DateTime.Now;

            var repoMock = new Mock<ISessionPriceRepository>();

            repoMock.Setup(r => r.GetAllSessionPrices(mockDate)).Returns(mockData);

            ISessionService sessionRepo = new SessionService(repoMock.Object);

            //ACTE 
            var firstCategory = sessionRepo.GetSessionViewModelForUI(mockDate).First().Category;
            var lastCategory = sessionRepo.GetSessionViewModelForUI(mockDate).Last().Category;

            //Assert
            Assert.AreEqual(firstCategory, "Catégorie 1");  // La categorie la plus chère
            Assert.AreEqual(lastCategory, "Carré Or");  // La categorie la moins chère

        }


    }
}
