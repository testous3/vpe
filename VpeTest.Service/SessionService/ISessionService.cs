﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VpeTest.Model;

namespace VpeTest.Service
{
    public interface ISessionService
    {
        IEnumerable<SessionPrice> GetSession(DateTime date);
        IEnumerable<SessionPriceViewModel> GetSessionViewModelForUI(DateTime date);
    }
}
