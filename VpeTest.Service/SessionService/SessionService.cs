﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VpeTest.Model;
using VpeTest.Repository;

namespace VpeTest.Service
{
    public class SessionService : ISessionService
    {

        private ISessionPriceRepository _sessionPriceRepository;
        public SessionService(ISessionPriceRepository sessionPriceRepository)
        {
            this._sessionPriceRepository = sessionPriceRepository;
        }
        public IEnumerable<SessionPrice> GetSession(DateTime date)
        {
            return _sessionPriceRepository.GetAllSessionPrices(date);
        }

        public IEnumerable<SessionPriceViewModel> GetSessionViewModelForUI(DateTime date)
        {
            var allSessionsPrices = this.GetSession(date).OrderByDescending(x => x.Price);
            var result = allSessionsPrices.GroupBy(s => s.Category).Select(
                x => new SessionPriceViewModel()
                {
                    Category = x.Key,
                    Zones = x.GroupBy(z => z.Zone)
                });
            return result;
        }


    }
}
