﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VpeTest.Model
{
    public class SessionPrice
    {
        public string Category { get; set; }
        public string Tarif { get; set; }
        public string Zone { get; set; }
        public int Price { get; set; }
        //public int Stock { get; set; }
        //public int OriginalPrice { get; set; }

    }


}
