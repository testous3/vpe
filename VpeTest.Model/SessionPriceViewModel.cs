﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VpeTest.Model
{
    public class SessionPriceViewModel
    {
        public string Category { get; set; }
        public IEnumerable<IGrouping<string, SessionPrice>> Zones { get; set; }
    }
}
