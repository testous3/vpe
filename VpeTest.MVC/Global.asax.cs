﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Unity;
using Unity.AspNet.Mvc;
using VpeTest.Repository;
using VpeTest.Service;

namespace VpeTest.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            RegisterComponents();
        }

        public void RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<ISessionPriceRepository, MockSessionPriceRepository>();
            container.RegisterType<ISessionService, SessionService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }


}
