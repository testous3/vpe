﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VpeTest.Model;

namespace VpeTest.Repository
{
    public interface ISessionPriceRepository
    {
        IEnumerable<SessionPrice> GetAllSessionPrices(DateTime date);
    }
}
