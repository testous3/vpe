﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VpeTest.Model;

namespace VpeTest.Repository
{
    public class MockSessionPriceRepository : ISessionPriceRepository
    {
        /// <summary>
        /// Cette methode retourne les données de l'exercices. 
        /// Pour info les données ne sont pas ordonnées.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public IEnumerable<SessionPrice> GetAllSessionPrices(DateTime date)
        {
            var mockData = new List<SessionPrice>();
            mockData.Add(new SessionPrice()
            {
                Category = "Catégorie 2",
                Tarif = "Normal",
                Zone = "Orchestre",
                Price = 37
            });
            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Normal",
                Zone = "Balcon",
                Price = 50
            });

            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Enfant",
                Zone = "Orchestre",
                Price = 43
            });
            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Enfant",
                Zone = "Balcon",
                Price = 45
            });

            mockData.Add(new SessionPrice()
            {
                Category = "Carré Or",
                Tarif = "Normal",
                Zone = "Orchestre",
                Price = 47
            });

            mockData.Add(new SessionPrice()
            {
                Category = "Catégorie 1",
                Tarif = "Normal",
                Zone = "Orchestre",
                Price = 45
            });              
            mockData.Add(new SessionPrice()
            {
                Category = "Catégorie 2",
                Tarif = "Enfant",
                Zone = "Orchestre",
                Price = 25
            });
            mockData.Add(new SessionPrice()
            {
                Category = "Catégorie 1",
                Tarif = "Enfant",
                Zone = "Orchestre",
                Price = 40
            });

            return mockData;
        }
    }
}
